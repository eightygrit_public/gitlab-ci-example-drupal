@api @d8

Feature: Test user authentication process

  Scenario: Title
    Given I am on the homepage
    Then I should see "Drupal"

  Scenario: Log in
    Given I visit "/user"
    # fill the username and password input fields, and click submit
    When I fill in "Username" with "admin"
    And I fill in "Password" with "pass"
    And I press the "Log in" button
    Then I should get a "200" HTTP response
    And I should see text matching "Log out"