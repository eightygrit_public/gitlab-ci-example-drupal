image: savoirfairelinux/lampd

stages:
  #- coding_standards
  - build
  - deploy

cache:
    key: "$CI_BUILD_REF_NAME"
    paths:
      - $CI_PROJECT_DIR/src/drupal
      - $HOME/.composer

#drupal_coding_standards:
#  stage: coding_standards
#  only:
#    - foobar
#  before_script:
#    - composer global require drupal/coder:^8.2.11 --no-ansi --no-progress
#    - composer global require dealerdirect/phpcodesniffer-composer-installer --no-ansi --no-progress
#    - ln -s $HOME/.composer/vendor/bin/phpcs /usr/bin/phpcs
#    - ln -s $HOME/.composer/vendor/bin/phpcbf /usr/bin/phpcbf
#    - ln -s $HOME/.composer/vendor/drupal/coder/coder_sniffer/DrupalPractice /root/.composer/vendor/squizlabs/php_codesniffer/CodeSniffer/Standards/DrupalPractice
#    - cd $HOME/.composer/vendor/drupal/coder && curl https://www.drupal.org/files/issues/2857856-8.patch | patch -p1
#    - phpcs --config-set installed_paths $HOME/.composer/vendor/drupal/coder/coder_sniffer
#  script:
#    - phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme $CI_PROJECT_DIR/www
#  allow_failure: true

build:
  stage: build
  only:
    - dev
  before_script:
    - ls -la
    - ls www
    - pwd
  script:
    - mv $CI_PROJECT_DIR/www $CI_PROJECT_DIR/www_tmp
    - drush make -y --translations=fr build-8.x.make $CI_PROJECT_DIR/www
    - cp -rf $CI_PROJECT_DIR/www_tmp/* $CI_PROJECT_DIR/www && rm -rf $CI_PROJECT_DIR/www_tmp
  artifacts:
    name: "plateform_${CI_BUILD_NAME}_${CI_BUILD_REF_NAME}_${CI_BUILD_REF}"
    expire_in: '1 week'
    paths:
      #- ./
      - www

.deploy template: &deploy_template
  stage: deploy
  environment:
    name:
    url:
  only:
    - trigger
  when: manual
  before_script:
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  script:
    # setup
    - export TARGETPATH="${TARGETBASEPATH}release/"
    - echo "Host    " $TARGETHOST
    - echo "Path    " $TARGETPATH
    # install deps with composer
    - ./Build/bin/composer.phar install --no-ansi --no-progress --no-dev
    - date > VERSION
    - echo "Tag   ${CI_BUILD_TAG}" >> VERSION
    - echo "Build ${CI_BUILD_ID}" >> VERSION
    - echo "Ref   ${CI_BUILD_REF}" >> VERSION
    - echo "Name  ${CI_BUILD_NAME}" >> VERSION
    - echo "git   $(git describe)" >> VERSION
    # prepare target host
    - ssh $TARGETHOST "mkdir -p $TARGETPATH"
    # - enable maintenance page
    #- touch web/maintenance.enable
    #- ssh $TARGETHOST "cd ${TARGETPATH}; mkdir -p ${TARGETPATH}web/; touch web/maintenance.enable"
    # sync files
    - rsync -azv -e "ssh" --delete
      --exclude=/composer.*
      --exclude=/shared
      --exclude=/Build
      --exclude=/README.md
      --exclude=/.ssh
      --exclude=.settings
      www $TARGETHOST:$TARGETPATH
    # finish build on target
    #- ssh $TARGETHOST "mkdir -p ${TARGETBASEPATH}shared/fileadmin"
    #- ssh $TARGETHOST "mkdir -p ${TARGETBASEPATH}shared/typo3temp"
    #- ssh $TARGETHOST "mkdir -p ${TARGETBASEPATH}shared/uploads"
    #- ssh $TARGETHOST "cd ${TARGETPATH}; test -L fileadmin || ln -s ${TARGETBASEPATH}shared/fileadmin"
    #- ssh $TARGETHOST "cd ${TARGETPATH}; test -L typo3temp || ln -s ${TARGETBASEPATH}shared/typo3temp"
    #- ssh $TARGETHOST "cd ${TARGETPATH}; test -L uploads || ln -s ${TARGETBASEPATH}shared/uploads"
    #- ssh $TARGETHOST "cd ${TARGETPATH}; ./typo3cms install:fixfolderstructure"
    #- ssh $TARGETHOST "cd ${TARGETPATH}; ./typo3cms cache:flush --force"
    #- ssh $TARGETHOST "cd ${TARGETPATH}; ./typo3cms database:updateschema '*.add,*.change' --verbose"
    #- ssh $TARGETHOST "cd ${TARGETPATH}; ./typo3cms migration:migrateAll"
    # finish/cleanup releases
    - echo "cleanup ... "
    #- ssh $TARGETHOST "cd ${TARGETPATH}; rm web/maintenance.enable"
    - echo "done."

Deploy:dev:
  <<: *deploy_template
  only:
    - dev
  variables:
    TARGETHOST: "deploy@dev.example.com"
    TARGETBASEPATH: "/tmp/dev.example.com/"
  environment:
    name: dev
    url: https://dev.example.com

Deploy:prod:
  <<: *deploy_template
  only:
    - master
  variables:
    TARGETHOST: "deploy@live.example.com"
    TARGETBASEPATH: "/tmp/www.example.com/"
  environment:
    name: live
    url: https://www.example.com
