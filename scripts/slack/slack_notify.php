<?php

// Load Slack helper functions
require_once( dirname( __FILE__ ) . '/slack_helper.php' );

// Assemble the Arguments
$slack_type = $argv[1]; // Argument One
$slack_channel = 'run-digital';

switch($slack_type) {
  case 'behat': 
    $slack_agent = 'Behat';
    $slack_icon = 'http://live-drupalcon-github-magic.pantheonsite.io/sites/default/files/icons/behat.png';
    $slack_color = '#0000000';
    $slack_message = 'Kicking off Behavioral Testing with Behat...';
    _slack_tell( $slack_message, $slack_channel, $slack_agent, $slack_icon, $slack_color);
    $slack_message = array();
    $slack_message[] = '*Scenario*: A user should see "El Museo de Arte" on the homepage' . "\n" . '     *Given* I am on the homepage' . "\n" .  '      *Then* I should see the text "El Museo de Arte"';
    _slack_tell( $slack_message, 'drupalcon', $slack_agent, $slack_icon, $slack_color);
    break;
  case 'behat_finished':
    $slack_agent = 'Behat';
    $slack_icon = 'http://live-drupalcon-github-magic.pantheonsite.io/sites/default/files/icons/behat.png';
    $slack_color = '#00ff00';
    $slack_message = 'Testing result: *Build PASSED*';
    _slack_tell( $slack_message, 'drupalcon', $slack_agent, $slack_icon, $slack_color);
    break;
  case 'composer':
    $slack_agent = 'Composer';
    #$slack_icon = 'http://live-drupalcon-github-magic.pantheonsite.io/sites/default/files/icons/composer.png';
    $slack_color = '#000080';
    $slack_message = 'Running Composer to create build artifact...';
    _slack_tell( $slack_message, 'drupalcon', $slack_agent, $slack_icon, $slack_color);
    $slack_message = array();
    $slack_message['Command'] = 'composer -n build-assets';
    $slack_message['Results'] = '121 installs, 0 updates, 0 removals';
    _slack_tell( $slack_message, 'drupalcon', $slack_agent, $slack_icon, $slack_color);
    break;
}
